<?php

/**
 * @file
 * uw_user_tutoring_profile.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_user_tutoring_profile_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_pattern_uw_tutoring_legal';
  $strongarm->value = 'Terms and conditions accepted by [current-user:cas:name]';
  $export['ant_pattern_uw_tutoring_legal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_php_uw_tutoring_legal';
  $strongarm->value = 0;
  $export['ant_php_uw_tutoring_legal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_uw_tutoring_legal';
  $strongarm->value = '1';
  $export['ant_uw_tutoring_legal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'cas_attributes';
  $strongarm->value = array(
    'sync_every_login' => '1',
    'overwrite' => '1',
    'relations' => array(
      'name' => '',
      'mail' => '[cas:ldap:mail]',
      'field_uw_user_guid' => '',
      'ldap_user_puid_sid' => '',
      'ldap_user_puid' => '[cas:ldap:objectguid]',
      'ldap_user_puid_property' => '',
      'ldap_user_current_dn' => '',
      'ldap_user_prov_entries' => '',
      'field_ldap_email' => '[cas:ldap:mail]',
      'field_ldap_fullname' => '[cas:ldap:givenname] [cas:ldap:sn]',
      'field_ldap_phone' => '[cas:ldap:telephonenumber]',
      'field_profile_student_id' => '',
    ),
    'roles' => array(
      'manage' => array(
        3 => 0,
        10 => 0,
        4 => 0,
        5 => 0,
        11 => 0,
        7 => 0,
        8 => 0,
        6 => 0,
        9 => 0,
      ),
      'mapping' => '',
    ),
    'ldap' => array(
      'server' => 'ldap-nexus',
    ),
  );
  $export['cas_attributes'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_uw_tutoring_legal';
  $strongarm->value = 0;
  $export['comment_anonymous_uw_tutoring_legal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_uw_tutoring_legal';
  $strongarm->value = 1;
  $export['comment_default_mode_uw_tutoring_legal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_uw_tutoring_legal';
  $strongarm->value = '50';
  $export['comment_default_per_page_uw_tutoring_legal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_uw_tutoring_legal';
  $strongarm->value = 1;
  $export['comment_form_location_uw_tutoring_legal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_uw_tutoring_legal';
  $strongarm->value = '1';
  $export['comment_preview_uw_tutoring_legal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_uw_tutoring_legal';
  $strongarm->value = 1;
  $export['comment_subject_field_uw_tutoring_legal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_uw_tutoring_legal';
  $strongarm->value = '1';
  $export['comment_uw_tutoring_legal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__uw_tutoring_legal';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'ical' => array(
        'custom_settings' => TRUE,
      ),
      'entity_teaser' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => FALSE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '2',
        ),
        'path' => array(
          'weight' => '5',
        ),
        'redirect' => array(
          'weight' => '4',
        ),
        'xmlsitemap' => array(
          'weight' => '3',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__uw_tutoring_legal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_uw_tutoring_legal';
  $strongarm->value = array();
  $export['menu_options_uw_tutoring_legal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_uw_tutoring_legal';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_uw_tutoring_legal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_limit_uw_tutoring_legal_access_arguments';
  $strongarm->value = array(
    0 => 'create',
    1 => 'uw_tutoring_legal',
  );
  $export['node_limit_uw_tutoring_legal_access_arguments'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_limit_uw_tutoring_legal_access_callback';
  $strongarm->value = 'node_access';
  $export['node_limit_uw_tutoring_legal_access_callback'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_uw_tutoring_legal';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_uw_tutoring_legal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_uw_tutoring_legal';
  $strongarm->value = '0';
  $export['node_preview_uw_tutoring_legal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_uw_tutoring_legal';
  $strongarm->value = 0;
  $export['node_submitted_uw_tutoring_legal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_uw_tutoring_legal_pattern';
  $strongarm->value = 'tutoring/legal/[node:title]';
  $export['pathauto_node_uw_tutoring_legal_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'xmlsitemap_settings_node_uw_tutoring_legal';
  $strongarm->value = array(
    'status' => '0',
    'priority' => '0.5',
  );
  $export['xmlsitemap_settings_node_uw_tutoring_legal'] = $strongarm;

  return $export;
}
