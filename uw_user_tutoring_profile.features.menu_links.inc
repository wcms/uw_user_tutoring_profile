<?php

/**
 * @file
 * uw_user_tutoring_profile.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_user_tutoring_profile_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-management_tutoring-all-users-report:admin/tutoring/tutoring-all-users-report.
  $menu_links['menu-site-management_tutoring-all-users-report:admin/tutoring/tutoring-all-users-report'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/tutoring/tutoring-all-users-report',
    'router_path' => 'admin/tutoring/tutoring-all-users-report',
    'link_title' => 'Tutoring all users report',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-site-management_tutoring-all-users-report:admin/tutoring/tutoring-all-users-report',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Exported menu link: menu-site-management_tutoring-import-courses:admin/config/system/import_courses.
  $menu_links['menu-site-management_tutoring-import-courses:admin/config/system/import_courses'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/import_courses',
    'router_path' => 'admin/config/system/import_courses',
    'link_title' => 'Tutoring import courses',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-site-management_tutoring-import-courses:admin/config/system/import_courses',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_tutoring-management:admin/tutoring/management.
  $menu_links['menu-site-management_tutoring-management:admin/tutoring/management'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/tutoring/management',
    'router_path' => 'admin/tutoring/management',
    'link_title' => 'Tutoring management',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-site-management_tutoring-management:admin/tutoring/management',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Exported menu link: menu-site-management_tutoring-notification-email:admin/tutoring/email.
  $menu_links['menu-site-management_tutoring-notification-email:admin/tutoring/email'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/tutoring/email',
    'router_path' => 'admin/tutoring/email',
    'link_title' => 'Tutoring notification email',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-site-management_tutoring-notification-email:admin/tutoring/email',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_tutoring-report:admin/tutoring/report.
  $menu_links['menu-site-management_tutoring-report:admin/tutoring/report'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/tutoring/report',
    'router_path' => 'admin/tutoring/report',
    'link_title' => 'Tutoring report',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-site-management_tutoring-report:admin/tutoring/report',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Exported menu link: menu-site-management_tutoring-revoke-tc:admin/tutoring/tutoring-revoke-tc.
  $menu_links['menu-site-management_tutoring-revoke-tc:admin/tutoring/tutoring-revoke-tc'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/tutoring/tutoring-revoke-tc',
    'router_path' => 'admin/tutoring/tutoring-revoke-tc',
    'link_title' => 'Tutoring revoke T&C',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-site-management_tutoring-revoke-tc:admin/tutoring/tutoring-revoke-tc',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Exported menu link: menu-site-management_tutoring-term-end:admin/tutoring/term-end.
  $menu_links['menu-site-management_tutoring-term-end:admin/tutoring/term-end'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/tutoring/term-end',
    'router_path' => 'admin/tutoring/term-end',
    'link_title' => 'Tutoring term end',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-site-management_tutoring-term-end:admin/tutoring/term-end',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Exported menu link: menu-site-manager-vocabularies_academic-year:admin/structure/taxonomy/uwaterloo_academic_year.
  $menu_links['menu-site-manager-vocabularies_academic-year:admin/structure/taxonomy/uwaterloo_academic_year'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uwaterloo_academic_year',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Academic year',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-site-manager-vocabularies_academic-year:admin/structure/taxonomy/uwaterloo_academic_year',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Exported menu link: menu-site-manager-vocabularies_courses:admin/structure/taxonomy/uwaterloo_courses.
  $menu_links['menu-site-manager-vocabularies_courses:admin/structure/taxonomy/uwaterloo_courses'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uwaterloo_courses',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Courses',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-site-manager-vocabularies_courses:admin/structure/taxonomy/uwaterloo_courses',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Exported menu link: menu-site-manager-vocabularies_hourly-rate:admin/structure/taxonomy/uwaterloo_hourly_rate.
  $menu_links['menu-site-manager-vocabularies_hourly-rate:admin/structure/taxonomy/uwaterloo_hourly_rate'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uwaterloo_hourly_rate',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Hourly rate',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-site-manager-vocabularies_hourly-rate:admin/structure/taxonomy/uwaterloo_hourly_rate',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Exported menu link: menu-site-manager-vocabularies_term:admin/structure/taxonomy/uwaterloo_term.
  $menu_links['menu-site-manager-vocabularies_term:admin/structure/taxonomy/uwaterloo_term'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uwaterloo_term',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Term',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-site-manager-vocabularies_term:admin/structure/taxonomy/uwaterloo_term',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Academic year');
  t('Courses');
  t('Hourly rate');
  t('Term');
  t('Tutoring all users report');
  t('Tutoring import courses');
  t('Tutoring management');
  t('Tutoring notification email');
  t('Tutoring report');
  t('Tutoring revoke T&C');
  t('Tutoring term end');

  return $menu_links;
}
