<?php

/**
 * @file
 * uw_user_tutoring_profile.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_user_tutoring_profile_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'admin tutoring'.
  $permissions['admin tutoring'] = array(
    'name' => 'admin tutoring',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_user_tutoring_profile',
  );

  // Exported permission: 'create field_ldap_email'.
  $permissions['create field_ldap_email'] = array(
    'name' => 'create field_ldap_email',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_ldap_fullname'.
  $permissions['create field_ldap_fullname'] = array(
    'name' => 'create field_ldap_fullname',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_ldap_phone'.
  $permissions['create field_ldap_phone'] = array(
    'name' => 'create field_ldap_phone',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_profile_address'.
  $permissions['create field_profile_address'] = array(
    'name' => 'create field_profile_address',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_profile_remember'.
  $permissions['create field_profile_remember'] = array(
    'name' => 'create field_profile_remember',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_tutoring_legal_accept'.
  $permissions['create field_tutoring_legal_accept'] = array(
    'name' => 'create field_tutoring_legal_accept',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_tutoring_legal_terms'.
  $permissions['create field_tutoring_legal_terms'] = array(
    'name' => 'create field_tutoring_legal_terms',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create uw_tutoring_legal content'.
  $permissions['create uw_tutoring_legal content'] = array(
    'name' => 'create uw_tutoring_legal content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_tutoring_legal content'.
  $permissions['delete any uw_tutoring_legal content'] = array(
    'name' => 'delete any uw_tutoring_legal content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_tutoring_legal content'.
  $permissions['delete own uw_tutoring_legal content'] = array(
    'name' => 'delete own uw_tutoring_legal content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in uwaterloo_term'.
  $permissions['delete terms in uwaterloo_term'] = array(
    'name' => 'delete terms in uwaterloo_term',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any uw_tutoring_legal content'.
  $permissions['edit any uw_tutoring_legal content'] = array(
    'name' => 'edit any uw_tutoring_legal content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit field_ldap_email'.
  $permissions['edit field_ldap_email'] = array(
    'name' => 'edit field_ldap_email',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_ldap_fullname'.
  $permissions['edit field_ldap_fullname'] = array(
    'name' => 'edit field_ldap_fullname',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_ldap_phone'.
  $permissions['edit field_ldap_phone'] = array(
    'name' => 'edit field_ldap_phone',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_profile_address'.
  $permissions['edit field_profile_address'] = array(
    'name' => 'edit field_profile_address',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_profile_remember'.
  $permissions['edit field_profile_remember'] = array(
    'name' => 'edit field_profile_remember',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_tutoring_legal_accept'.
  $permissions['edit field_tutoring_legal_accept'] = array(
    'name' => 'edit field_tutoring_legal_accept',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_tutoring_legal_terms'.
  $permissions['edit field_tutoring_legal_terms'] = array(
    'name' => 'edit field_tutoring_legal_terms',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_ldap_email'.
  $permissions['edit own field_ldap_email'] = array(
    'name' => 'edit own field_ldap_email',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_ldap_fullname'.
  $permissions['edit own field_ldap_fullname'] = array(
    'name' => 'edit own field_ldap_fullname',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_ldap_phone'.
  $permissions['edit own field_ldap_phone'] = array(
    'name' => 'edit own field_ldap_phone',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_profile_address'.
  $permissions['edit own field_profile_address'] = array(
    'name' => 'edit own field_profile_address',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_profile_remember'.
  $permissions['edit own field_profile_remember'] = array(
    'name' => 'edit own field_profile_remember',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_tutoring_legal_accept'.
  $permissions['edit own field_tutoring_legal_accept'] = array(
    'name' => 'edit own field_tutoring_legal_accept',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_tutoring_legal_terms'.
  $permissions['edit own field_tutoring_legal_terms'] = array(
    'name' => 'edit own field_tutoring_legal_terms',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own uw_tutoring_legal content'.
  $permissions['edit own uw_tutoring_legal content'] = array(
    'name' => 'edit own uw_tutoring_legal content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in uwaterloo_term'.
  $permissions['edit terms in uwaterloo_term'] = array(
    'name' => 'edit terms in uwaterloo_term',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'execute views_bulk_operations_delete_item'.
  $permissions['execute views_bulk_operations_delete_item'] = array(
    'name' => 'execute views_bulk_operations_delete_item',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'actions_permissions',
  );

  // Exported permission: 'execute views_bulk_operations_delete_revision'.
  $permissions['execute views_bulk_operations_delete_revision'] = array(
    'name' => 'execute views_bulk_operations_delete_revision',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'actions_permissions',
  );

  // Exported permission: 'view field_ldap_email'.
  $permissions['view field_ldap_email'] = array(
    'name' => 'view field_ldap_email',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_ldap_fullname'.
  $permissions['view field_ldap_fullname'] = array(
    'name' => 'view field_ldap_fullname',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_ldap_phone'.
  $permissions['view field_ldap_phone'] = array(
    'name' => 'view field_ldap_phone',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_profile_address'.
  $permissions['view field_profile_address'] = array(
    'name' => 'view field_profile_address',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_profile_remember'.
  $permissions['view field_profile_remember'] = array(
    'name' => 'view field_profile_remember',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_tutoring_legal_accept'.
  $permissions['view field_tutoring_legal_accept'] = array(
    'name' => 'view field_tutoring_legal_accept',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_tutoring_legal_terms'.
  $permissions['view field_tutoring_legal_terms'] = array(
    'name' => 'view field_tutoring_legal_terms',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_ldap_email'.
  $permissions['view own field_ldap_email'] = array(
    'name' => 'view own field_ldap_email',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_ldap_fullname'.
  $permissions['view own field_ldap_fullname'] = array(
    'name' => 'view own field_ldap_fullname',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_ldap_phone'.
  $permissions['view own field_ldap_phone'] = array(
    'name' => 'view own field_ldap_phone',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_profile_address'.
  $permissions['view own field_profile_address'] = array(
    'name' => 'view own field_profile_address',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_profile_remember'.
  $permissions['view own field_profile_remember'] = array(
    'name' => 'view own field_profile_remember',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_tutoring_legal_accept'.
  $permissions['view own field_tutoring_legal_accept'] = array(
    'name' => 'view own field_tutoring_legal_accept',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_tutoring_legal_terms'.
  $permissions['view own field_tutoring_legal_terms'] = array(
    'name' => 'view own field_tutoring_legal_terms',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
