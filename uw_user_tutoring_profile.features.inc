<?php

/**
 * @file
 * uw_user_tutoring_profile.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_user_tutoring_profile_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "ldap_servers" && $api == "ldap_servers") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_user_tutoring_profile_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_user_tutoring_profile_node_info() {
  $items = array(
    'uw_tutoring_legal' => array(
      'name' => t('Tutoring legal'),
      'base' => 'node_content',
      'description' => t('Terms and conditions for tutoring'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
