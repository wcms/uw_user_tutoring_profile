/**
 * @file
 */

(function ($) {

// Set full name and email fields readonly.
$(function () {
  $("#edit-field-ldap-fullname-und-0-value").attr('readonly','readonly');
  $("#edit-field-ldap-email-und-0-value").attr('readonly','readonly');
});

}(jQuery));
