<?php

/**
 * @file
 * uw_user_tutoring_profile.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_user_tutoring_profile_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_tutor_profile|user|user|form';
  $field_group->group_name = 'group_tutor_profile';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Register as a tutor',
    'weight' => '0',
    'children' => array(
      0 => 'field_ldap_fullname',
      1 => 'field_ldap_phone',
      2 => 'field_profile_student_id',
      3 => 'field_profile_remember',
      4 => 'field_profile_address',
      5 => 'field_profile_hourly_rate',
      6 => 'field_profile_academic_year',
      7 => 'field_profile_term',
      8 => 'field_profile_addtional_info',
      9 => 'field_profile_status',
      10 => 'field_profile_help_text',
      11 => 'field_profile_info_help',
      12 => 'field_profile_view_msg',
      13 => 'field_profile_courses',
      14 => 'field_ldap_email',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_tutor_profile|user|user|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Register as a tutor');

  return $field_groups;
}
