<?php

/**
 * @file
 * uw_user_tutoring_profile.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function uw_user_tutoring_profile_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_ldap_email'.
  $field_bases['field_ldap_email'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_ldap_email',
    'field_permissions' => array(
      'type' => 2,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'max_length' => 255,
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_ldap_fullname'.
  $field_bases['field_ldap_fullname'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_ldap_fullname',
    'field_permissions' => array(
      'type' => 2,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'max_length' => 255,
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_ldap_phone'.
  $field_bases['field_ldap_phone'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_ldap_phone',
    'field_permissions' => array(
      'type' => 2,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'max_length' => 255,
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_profile_academic_year'.
  $field_bases['field_profile_academic_year'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_profile_academic_year',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 1,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'uwaterloo_academic_year' => 'uwaterloo_academic_year',
        ),
      ),
      'profile2_private' => FALSE,
      'target_type' => 'taxonomy_term',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_profile_address'.
  $field_bases['field_profile_address'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_profile_address',
    'field_permissions' => array(
      'type' => 2,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_profile_addtional_info'.
  $field_bases['field_profile_addtional_info'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_profile_addtional_info',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_profile_courses'.
  $field_bases['field_profile_courses'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_profile_courses',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 1,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'uwaterloo_courses' => 'uwaterloo_courses',
        ),
      ),
      'profile2_private' => FALSE,
      'target_type' => 'taxonomy_term',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_profile_help_text'.
  $field_bases['field_profile_help_text'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_profile_help_text',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'markup' => array(
        'format' => 'uw_tf_standard',
        'value' => '<p>We will not share your phone number or address, and it will not show up in your tutor profile when other students search for a tutor. We collect this information to ensure your safety.</p>
',
      ),
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_profile_hourly_rate'.
  $field_bases['field_profile_hourly_rate'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_profile_hourly_rate',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 1,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'uwaterloo_hourly_rate' => 'uwaterloo_hourly_rate',
        ),
      ),
      'profile2_private' => FALSE,
      'target_type' => 'taxonomy_term',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_profile_info_help'.
  $field_bases['field_profile_info_help'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_profile_info_help',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'markup' => array(
        'format' => 'uw_tf_standard',
        'value' => '<p>Use this to let students know anything extra you think they might need to know. For example, let students know about your experience as a tutor or if you can only tutor during certain hours or at certain locations.</p>
',
      ),
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_profile_remember'.
  $field_bases['field_profile_remember'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_profile_remember',
    'field_permissions' => array(
      'type' => 2,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'markup' => array(
        'format' => 'uw_tf_standard',
        'value' => '<p>You only need to create your tutor profile once. At any point if you need to stop tutoring for any reason, set your status to not available and you will no longer show up in the search.</p>
<p>At the end of the term we reset all tutor profiles to not available. If you would like to continue tutoring for the next term log in to Tutor Connect starting from the first day of classes to set your status as available and select the courses you want to tutor in the new term. If you&rsquo;re away from campus for the term or just don&rsquo;t want to tutor for a term your status can stay as not available. This helps us ensure the list of tutors is kept up-to-date.</p>
<p>Happy tutoring!</p>
<p>Contact: <a href="mailto:ssocoach@uwaterloo.ca">success@uwaterloo.ca</a></p>
',
      ),
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_profile_status'.
  $field_bases['field_profile_status'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_profile_status',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Available' => 'Available',
        'Not available' => 'Not available',
        'End of term' => 'End of term',
      ),
      'allowed_values_function' => '',
      'entity_translation_sync' => FALSE,
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_profile_term'.
  $field_bases['field_profile_term'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_profile_term',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 1,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'uwaterloo_term' => 'uwaterloo_term',
        ),
      ),
      'profile2_private' => FALSE,
      'target_type' => 'taxonomy_term',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_profile_view_msg'.
  $field_bases['field_profile_view_msg'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_profile_view_msg',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'markup' => array(
        'format' => 'uw_tf_standard',
        'value' => '<p>This is the information that students will see when viewing your tutor profile.</p>
',
      ),
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_tutoring_legal_accept'.
  $field_bases['field_tutoring_legal_accept'] = array(
    'active' => 1,
    'cardinality' => 2,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_tutoring_legal_accept',
    'field_permissions' => array(
      'type' => 2,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Yes' => 'Yes',
      ),
      'allowed_values_function' => '',
      'entity_translation_sync' => FALSE,
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_tutoring_legal_terms'.
  $field_bases['field_tutoring_legal_terms'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_tutoring_legal_terms',
    'field_permissions' => array(
      'type' => 2,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'markup' => array(
        'format' => 'uw_tf_standard',
        'value' => '<h2>Terms and Conditions</h2>
<h3>Each term we ask that you review and accept the following Terms and Conditions for use of Tutor Connect.</h3>
<p>By accessing the University of Waterloo (&ldquo;UW&rdquo;) Student Success Office (the &ldquo;SSO&rdquo;) Tutor Connect online posting board (&ldquo;Tutor Connect&rdquo;), you acknowledge and agree to the following terms and conditions:</p>
<ol>
	<li>
		<p>The contents of Tutor Connect (the &ldquo;Contents&rdquo;):</p>
		<ol type="a">
			<li>
				<p>are to be used by UW students (&ldquo;Students&rdquo;) only for the following purposes (the &ldquo;Authorized Purposes&rdquo;):</p>
				<ol type="i">
					<li>
						<p>offering tutoring services to other Students; or</p>
					</li>
					<li>
						<p>searching for and obtaining tutoring services offered by other Students.</p>
					</li>
				</ol>
			</li>
			<li>
				<p>include intellectual property of UW and its licensors and personal information of Students; and</p>
			</li>
			<li>
				<p>are protected by law, including, but without limitation, intellectual property, privacy and contract law.</p>
			</li>
		</ol>
	</li>
	<li>
		<p>UW employees may also access Tutor Connect for purposes related to administration of Tutor Connect, including, but without limitation, contacting Students who offer tutoring services (the &ldquo;Tutors&rdquo;), as necessary, and addressing feedback, questions and complaints from Students using Tutor Connect.</p>
	</li>
	<li>
		<p>You shall not and shall not allow others to:</p>
		<ol type="a">
			<li>
				<p>Access, use, copy, display or download the Contents for any purpose other than the Authorized Purpose for which you have accessed Tutor Connect;</p>
			</li>
			<li>
				<p>Access or attempt to access the underlying source code, systems, databases, networks or servers comprising or supporting Tutor Connect, for any reason whatsoever;</p>
			</li>
			<li>
				<p>Without limiting the generality of the foregoing, you shall not and shall not allow others to:</p>
				<ol type="i">
					<li>
						<p>share, sell, lease, distribute, copy, display, download or attempt to do any of the foregoing with respect to the Contents for any commercial or other purpose not authorized in writing by UW;</p>
					</li>
					<li>
						<p>modify, remove or attempt to do any of the foregoing with respect to the Content, except for a Tutor who may modify or remove Content posted by that Tutor; or</p>
					</li>
					<li>
						<p>negatively affect the functioning, use, confidentiality or security of the Contents, Tutor Connect, systems on which Tutor Connect is hosted or any of UW&rsquo;s or its licensors&rsquo; or service providers&rsquo; systems, including, but without limitation, by interfering with; disabling; overburdening; damaging; impairing; abusing; transmitting harmful files or programs to or through; using another person&rsquo;s user ID and password; attempting to gain unauthorized access to; using any program, device or process to monitor, copy or extract Content from; or attempting to do any of the foregoing with respect to Tutor Connect, the Contents, systems on which Tutor Connect is hosted or any of UW&rsquo;s or its licensors&rsquo; or service providers&rsquo; systems.</p>
					</li>
				</ol>
			</li>
			<li>
				<p>Post materials that: (i) violate any applicable laws; (ii) are harmful, false, inaccurate, defamatory, offensive, discriminatory or misleading; or (iii) infringe a third party&rsquo;s privacy rights or intellectual property rights.</p>
			</li>
		</ol>
	</li>
	<li>
		<p>You are responsible for:</p>
		<ol type="a">
			<li>
				<p>keeping your user ID, password and personal information confidential;</p>
			</li>
			<li>
				<p>taking steps to ensure your personal safety as a Tutor or Student receiving tutoring services (a &ldquo;Tutee&rdquo;), as applicable;</p>
			</li>
			<li>
				<p>compliance with applicable laws and UW&rsquo;s policies, including, but without limitation, privacy law and policies, Policy 33 &ndash;Ethical Behaviour, Policy 34 &ndash; Health, Safety and Environment, Policy 71 &ndash; Student Discipline, Guidelines on Use of Waterloo Computing and Network Resources and the Statement on Security of UW Computing and Network Resources;</p>
			</li>
			<li>
				<p>the form, content, accuracy and completeness of all material you post to Tutor Connect and all other information you share with Tutors or Tutees, as applicable;</p>
			</li>
			<li>
				<p>appointments, financial arrangements and disputes between you and your Tutee or Tutor, as applicable; and</p>
			</li>
			<li>
				<p>making your own decisions regarding offering, obtaining or discontinuing tutoring services, as applicable.</p>
			</li>
		</ol>
	</li>
	<li>
		<p>Terms Specific to Tutors:</p>
		<ol type="a">
			<li>
				<p>If a Student wishes to be a Tutor, s/he must log into Tutor Connect and fill out an online form with certain required information, including, but without limitation, full name, student number, telephone number, courses for which s/he is offering tutoring, academic term during which s/he is offering tutoring, rate of pay per hour, status (available/unavailable), student email address and additional notes regarding availability and services offered.</p>
			</li>
			<li>
				<p>Tutors acknowledge and agree that the information they post, except telephone numbers and student identification numbers, will be available to everyone who has access to Tutor Connect.</p>
			</li>
			<li>
				<p>Tutors acknowledge and agree that their telephone numbers, student identification numbers and the date they accepted the terms of use will be available to UW employees who have administrator access to Tutor Connect and may be used for the purposes of administering Tutor Connect, as described in Section 2 above.</p>
			</li>
			<li>
				<p>Tutors acknowledge and agree that their profiles on Tutor Connect will be set to &ldquo;not available&rdquo; at the end of every semester. If a Tutor wishes to continue to offer tutoring services, the Tutor must log onto Tutor Connect at the beginning of every semester to reset their availability to &ldquo;available&rdquo;.</p>
			</li>
		</ol>
	</li>
	<li>
		<p>You acknowledge and agree that UW is providing the Tutor Connect platform solely as a convenience to Students. As such, UW is not responsible for:</p>
		<ol type="a">
			<li>
				<p>reviewing, screening or monitoring the Content, the Tutors or the Tutees;</p>
			</li>
			<li>
				<p>ensuring your personal safety as a Tutor or Tutee, as applicable;</p>
			</li>
			<li>
				<p>appointments, financial arrangements or disputes, between Tutors and Tutees;</p>
			</li>
			<li>
				<p>tutoring services, including, but without limitation, the content, accuracy, completeness, quality or efficacy of the tutoring services; or</p>
			</li>
			<li>
				<p>academic results achieved by Tutees.</p>
			</li>
		</ol>
	</li>
	<li>
		<p>UW makes no representations, warranties or conditions (statutory or otherwise) about Tutor Connect, the Contents, the Tutors, the Tutees or the tutoring services, all of which is provided on an &ldquo;AS IS&rdquo; basis. UW is not responsible or liable for any loss whatsoever, including but without limitation indirect, special, exemplary, punitive, consequential or other damages arising from, in connection with or otherwise related to the use of, access to or inability to access Tutor Connect, the Contents, the Tutors, the Tutees or tutoring services.</p>
	</li>
	<li>
		<p>A breach of these terms of use, as determined by UW in its sole discretion, may result in a Tutor profile being removed, a Student being barred from accessing Tutor Connect, penalties under UW Policy 71 &ndash; Student Discipline or legal action.</p>
	</li>
</ol>
',
      ),
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  return $field_bases;
}
