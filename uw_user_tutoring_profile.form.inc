<?php

/**
 * @file
 * Forms.
 */

/**
 * Import form.
 *
 * Term ID: The first three digits are the year of the term minus 1900. The last
 * digit is the month during which it starts: 1 = winter, 5 = spring, 9 = fall.
 */
function uw_user_tutoring_profile_import_form($form, &$form_state) {
  // Set up dates and determine current term.
  $month = date('n');
  $year = date('y');
  $term_name['1'] = 'Winter';
  $term_name['5'] = 'Spring';
  $term_name['9'] = 'Fall';
  if ($month >= 9) {
    $current_term = '9';
  }
  elseif ($month >= 5) {
    $current_term = '5';
  }
  else {
    $current_term = '1';
  }

  // Get current term year, e.g Fall 2017 for message use.
  $current_term_id = '1' . $year . $current_term;
  $terms[$current_term_id]  = $term_name[$current_term] . ' 20' . $year;

  // Increment year (if needed) and term number.
  if ($current_term == '9') {
    $year++;
  }
  if ($current_term == '9') {
    $next_term = '1';
  }
  elseif ($current_term == '5') {
    $next_term = '9';
  }
  else {
    $next_term = '5';
  }

  // Build info for the coming soon term.
  $next_term_id = '1' . $year . $next_term;
  $terms[$next_term_id] = $term_name[$next_term] . ' 20' . $year;

  if (!isset($form_state['storage']['confirm'])) {
    $form['field_course_term'] = array(
      '#type' => 'select',
      '#title' => t('Term') . ': ',
      '#default_value' => array($next_term_id),
      '#options' => $terms,
      '#required' => TRUE,
    );

    $form['current_term'] = array(
      '#type' => 'hidden',
      '#value' => $terms[$current_term_id],
    );
    $form['next_term'] = array(
      '#type' => 'hidden',
      '#value' => $terms[$next_term_id],
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );
    return $form;
  }
  else {
    $message = check_plain("Are you sure you want to remove the existing courses and import " . $terms[$form_state['input']['field_course_term']] . "?");
    $description = check_plain("This will delete all existing courses and import all new courses from " . $terms[$form_state['input']['field_course_term']] . " into the Courses vocabulary.  This action can not be undone.");
    $description = "<p>" . $description . "</p>";
    $form['field_course_term'] = array(
      "#type" => "hidden",
      "#title" => "course_term",
      "#value" => $form_state['input']['field_course_term'],
    );
    $form['current_term'] = array(
      "#type" => "hidden",
      "#value" => $form_state['input']['current_term'],
    );
    $form['next_term'] = array(
      "#type" => "hidden",
      "#value" => $form_state['input']['next_term'],
    );
    return confirm_form($form, $message, 'admin/config/system/import_courses', $description, 'Confirm', 'Cancel');
  }
}

/**
 * Form submit handler.
 */
function uw_user_tutoring_profile_import_form_submit($form, &$form_state) {
  if (!isset($form_state['storage']['confirm'])) {
    $form_state['storage']['confirm'] = TRUE;
    $form_state['rebuild'] = TRUE;
  }
  else {
    // These are the settings to use in the batch of the delete graduate courses.
    // start is for what record to start from, these will be 0, unless extreme circumstances.
    $settings['course_term'] = $form_state['input']['field_course_term'];
    $settings['api_key'] = variable_get('uw_open_api_key');
    $settings['limit'] = 20;
    $settings['current_term'] = $form_state['input']['current_term'];
    $settings['next_term'] = $form_state['input']['next_term'];
    $settings['select_term'] = uw_value_lists_uw_term_number_to_string($form_state['input']['field_course_term']);

    $operations = array(
      array('_uw_user_tutoring_profile_batch_delete_courses', array($settings)),
      array('_uw_user_tutoring_profile_batch_import_courses_after_delete', array($settings)),
    );

    // This is the batch array from drupal core.
    $batch = array(
      'operations' => $operations,
      'title' => t('Batch delete and import courses'),
      'init_message' => t('Process is starting ...'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('batch process has encountered an error.'),
      'finished' => '_uw_user_tutoring_profile_batch_import_finished',
    );

    // Start the batch process.
    batch_set($batch);
  }
}
