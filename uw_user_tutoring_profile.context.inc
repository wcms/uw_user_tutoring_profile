<?php

/**
 * @file
 * uw_user_tutoring_profile.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_user_tutoring_profile_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'tutoring_logout_block';
  $context->description = 'Display logout link on the sidebar';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'tutoring' => 'tutoring',
        'tutoring/*' => 'tutoring/*',
        'users/*' => 'users/*',
        'user/*' => 'user/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_user_tutoring_profile-logout' => array(
          'module' => 'uw_user_tutoring_profile',
          'delta' => 'logout',
          'region' => 'sidebar_second',
          'weight' => '-47',
        ),
        'uw_user_tutoring_profile-logout_image' => array(
          'module' => 'uw_user_tutoring_profile',
          'delta' => 'logout_image',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Display logout link on the sidebar');
  $export['tutoring_logout_block'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'tutoring_search_block';
  $context->description = 'Display tutor search under tutoring/search page';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'tutoring/search' => 'tutoring/search',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views--exp-tutoring_search-page' => array(
          'module' => 'views',
          'delta' => '-exp-tutoring_search-page',
          'region' => 'content',
          'weight' => '9',
        ),
        'service_links-service_links' => array(
          'module' => 'service_links',
          'delta' => 'service_links',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Display tutor search under tutoring/search page');
  $export['tutoring_search_block'] = $context;

  return $export;
}
